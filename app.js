var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var session = require('express-session');
var cors = require('cors');
var expressValidator = require('express-validator');
const expressOasGenerator = require('express-oas-generator');
const _ = require('lodash');

var uporabnik = require('./routes/uporabnik');
var tip_uporabnika = require('./routes/tip_uporabnika');
var obvestilo = require('./routes/obvestilo');
var lovska_druzina = require('./routes/lovska_druzina');
var lovisce = require('./routes/lovisce');
var lov = require('./routes/lov');
var lovec_lov = require('./routes/lovec-lov');

var app = express();
expressOasGenerator.init(app, function(spec) {
  _.set(spec, 'paths["/{name}"].get.parameters[0].description', 'description of a parameter');
  return spec;
});

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(cors());
//app.use(expressValidator());



const {
  SESS_LIFETIME = 1000 * 60 * 60 * 2,
  NODE_ENV = 'development',
  SESS_NAME = 'sid',
  SESS_SECRET = 'shh!itsasecret'
} = process.env

// Connection to MySQL database "lovci"
var mysql = require("mysql");
/* var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "root",
  database: "lovci"
}); */
var con = mysql.createConnection({
  host: "sql7.freesqldatabase.com",
  user: "sql7324669",
  password: "djYFfJ2wMf",
  database: "sql7324669"
});

con.connect(function(err){
  if(err){
    console.log(err);
    console.log('Error connecting to Db');
    return;
  }
  console.log('Connection established');
});

// Make connection global
app.use(function(req,res,next){
  req.con = con;
  next();
});

const IN_PROD = NODE_ENV === 'production';

app.use(session({
  name: SESS_NAME,
  resave: false,
  saveUninitialized: false,
  secret: SESS_SECRET,
  cookie: {
    secure: IN_PROD,
    maxAge: SESS_LIFETIME,
    sameSite: true
  }
}))

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

/* app.get('/', (req, res) => {const { userId } = req.session}); */

app.use('/uporabnik', uporabnik);
app.use('/tip', tip_uporabnika);
app.use('/obvestilo', obvestilo);
app.use('/druzina', lovska_druzina);
app.use('/lovisce', lovisce);
app.use('/lov', lov);
app.use('/lovec_lov', lovec_lov);

app.use(function (err, req, res, next) {
  console.error(err.message);
  if (!err.statusCode) err.statusCode = 500;
  res.status(err.statusCode).send(err.message);
});

module.exports = app;
