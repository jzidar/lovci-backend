const { body } = require('express-validator')

class UporabnikService {
    static currentPassword(req, callback) {
        var userId = req.params.id;
        var db = req.con;
        db.query("SELECT password \
              FROM uporabnik \
              WHERE uporabnik_id = ?", [userId], (error, result, fields) => {
                if(error) throw error;
                if(!result.length) {
                  console.log("Uporabnik ne obstaja.");
                } else {                  
                  callback(JSON.parse(JSON.stringify(result))[0]);
                }
                });
    }

    static validate(method) {
        switch (method) {
            case 'noviUporabnik': {
             return [ 
                body('ime')
                    .exists().withMessage("Vnos je obvezen.")
                    .isAlpha().withMessage("Ime ne sme vsebovati številk.")
                    .isLength({min: 3, max: 20}).withMessage("Ime mora biti dolgo vsaj 3 znake in največ 20 znakov."),
                body('priimek')
                    .exists().withMessage("Vnos je obvezen.")
                    .isAlpha().withMessage("Priimek ne sme vsebovati številk.")
                    .isLength({min: 3, max: 20}).withMessage("Priimek mora biti dolg vsaj 3 znake in največ 20 znakov."),
                body('email')
                    .exists().withMessage("Vnos je obvezen.")
                    .isEmail().withMessage("Vnesi veljaven e-poštni naslov."),
                body('password1')
                    .exists().withMessage("Vnos je obvezen.")
                    .isLength({min: 6, max: 20}).withMessage("Geslo mora biti dolgo vsaj 6 znakov in največ 20 znakov."),
                body('telefonska')
                    .exists().withMessage("Vnos je obvezen.")
                    .matches("[0][3457]{1}[01][0-9]{3}[0-9]{3}").withMessage("Vnesi veljavno tel. št. Primer: 051852115")
               ]   
            }
            case 'posodobitevUporabnika': {
            return [
                body('old_password')
                    .exists().withMessage("Vnos je obvezen.")
                    .matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})")
                    .withMessage("Geslo mora vsebovati najmanj 8 znakov, od tega vsaj en posebni znak in številko.")
                    .custom((old_password, { req }) => {
                        this.currentPassword(req, function(pass) {
                            if(old_password !== pass.password) return false;//console.log("Vneseno staro geslo ni pravilno.");
                        });
                        return true;                    
                    }).withMessage("Vneseno staro geslo ni pravilno."),
                body('new_password')
                    .exists().withMessage("Vnos je obvezen.")
                    .matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})")
                    .withMessage("Geslo mora vsebovati najmanj 8 znakov, od tega vsaj en posebni znak in številko.")
                    .custom((new_password, { req }) => {
                        if(new_password === req.body.old_password) throw new Error("Novo geslo ne sme biti enako prejšnjemu.");
                        else return true;
                    }),
                body('conf_new_password')
                    .exists().withMessage("Vnos je obvezen.")
                    .matches("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})")
                    .withMessage("Geslo mora vsebovati najmanj 8 znakov, od tega vsaj en posebni znak in številko.")
                    .custom((conf_new_password, { req }) => {
                        if(conf_new_password !== req.body.new_password) throw new Error("Potrditveno geslo se mora ujemati z novim geslom."); 
                        else return true;
                    })
                ]   
            }
        }
    }
}

module.exports = UporabnikService;
