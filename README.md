# Spletna aplikacija Elovec

## Tehnologija programske opreme 2019/20


Aplikacija je dostopna na spletnem naslovu: http://elovec.cf

Za testiranje aplikacije se lahko vanjo prijavite z računi:

uporabnik: 'uporabnik@test.com' (družina 1)

uporabnik: 'uporabnik2@test.com' (družina 2)

admin: 'admin@test.com' (admin družine 1)

geslo za vse: 'Banana 123'


### Backend

Je napisan v Nodejs in stoji na https://www.openshift.com/

Baza je MySQL in stoji na https://www.freemysqlhosting.net/