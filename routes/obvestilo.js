var express = require('express');
var router = express.Router();

router.get('/all', function(req, res) {
  var db = req.con;
  db.query("SELECT * \
            FROM obvestilo \
            WHERE druzina_id is null", (error, result, fields) => {
              if(result && !result.length) {
                res.status(406).send("Podatkov ni bilo mogoče dobiti.");
                console.log("Podatkov ni bilo mogoče dobiti.");  
              } else {
                res.json( JSON.parse(JSON.stringify(result)) ); 
              }
              });
});

router.get('/all_admin/:id', function(req, res) {
  var db = req.con;
  const id = req.params.id;

  db.query("SELECT * \
            FROM obvestilo \
            WHERE druzina_id is null OR druzina_id = ?", [id],(error, result, fields) => {
              if(!result.length) {
                res.status(406).send("Podatkov ni bilo mogoče dobiti.");
                console.log("Podatkov ni bilo mogoče dobiti.");  
              } else {
                res.json( JSON.parse(JSON.stringify(result)) ); 
              }
              });
});

router.get('/:id', function(req, res) {
  const id = req.params.id;
  var db = req.con;
  db.query("SELECT * \
            FROM obvestilo \
            WHERE druzina_id = ?", [id], (error, result, fields) => {
                if(!result.length) {
                  if(error == null) {
                    res.status(201).send("Ni obvestil.");
                    return;
                  }
                  res.status(406).send("Podatkov ni bilo mogoče dobiti.");
                  console.log("Podatkov ni bilo mogoče dobiti.");  
                } else {
                  res.json( result); 
                }
            });
});

router.get('/rm/:id', function(req, res) {
  const id = req.params.id;
  var db = req.con;
  db.query("DELETE FROM obvestilo WHERE obvestilo_id = ?", [id], (error, result, fields) => {
                if(!result.length) {
                  res.status(200).send("Odstranjeno.");
                }
            });
});

router.post('/add', function(req, res) {
  var db = req.con;
  const { uporabnik_id, druzina_id, naslov, obvestilo } = req.body;
  const cas_nastanka = new Date().toISOString().slice(0, 19).replace('T', ' ');
  const zadnja_sprememba = new Date().toISOString().slice(0, 19).replace('T', ' ');

  db.query("INSERT INTO obvestilo SET ?", [{uporabnik_id, druzina_id, naslov, obvestilo, cas_nastanka, zadnja_sprememba}], (error, result, fields) => {
              if(error) {
                res.status(406).send("Napaka.");
              } else {
                res.status(201).send("" + result.insertId);
              }
      });
});
/* 
router.post('/update/:id', function(req, res) {
  const id = req.params.id;
  var db = req.con;
  const { uporabnik_id, druzina_id, naslov, obvestilo } = req.body;
  const zadnja_sprememba = new Date().toISOString().slice(0, 19).replace('T', ' ');

  db.query("UPDATE obvestilo \
            SET ? \
            WHERE obvestilo_id = ?", [{uporabnik_id, druzina_id, naslov, obvestilo, zadnja_sprememba}, id], (error, result, fields) => {
              if(error) {
                console.log(error);
                res.status(406).send("Napaka.");
              } else {
                res.status(201).send("");
              }
      });
}); */

router.post('/update/:id', function(req, res) {
  const id = req.params.id;
  var db = req.con;
  const { naslov, obvestilo } = req.body;
  const zadnja_sprememba = new Date().toISOString().slice(0, 19).replace('T', ' ');
console.log(req.body);
  db.query("UPDATE obvestilo \
            SET ? \
            WHERE obvestilo_id = ?", [{naslov, obvestilo, zadnja_sprememba}, id], (error, result, fields) => {
              if(error) {
                console.log(error);
                res.status(406).send("Napaka.");
              } else {
                res.status(201).send("");
              }
      });
});

module.exports = router;
