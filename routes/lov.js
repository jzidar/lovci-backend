var express = require('express');
var router = express.Router();

router.get('/all/:id', function(req, res) {
    var db = req.con;
    const id = req.params.id;

    db.query("SELECT lov.lov_id, lovisce.naziv_lovisca, lov.zacetek, lov.konec, lov.plen, lovec_na_lovu.st_strelov \
            FROM `lov` \
            JOIN lovec_na_lovu on lovec_na_lovu.lov_lid = lov.lov_id \
            JOIN uporabnik on uporabnik.uporabnik_id = lovec_na_lovu.uporabnik_id \
            JOIN lovisce on lovisce.lovisce_id = lov.lovisce_id \
            WHERE uporabnik.druzina_id = ?", [id], (error, result, fields) => {
                if(!result.length) {
                  res.status(406).send("Podatkov ni bilo mogoče dobiti.");
                } else {
                  res.json( JSON.parse(JSON.stringify(result)) ); 
                }
                });
  });

router.get('/allHunters/:id', function(req, res) {
var db = req.con;
const id = req.params.id;

db.query("SELECT * \
            FROM lov \
            JOIN lovec_na_lovu USING(lov_id) \
            WHERE lov.lov_id = ?", [id], (error, result, fields) => {
            if(!result.length) {
                res.status(406).send("Podatkov ni bilo mogoče dobiti.");
            } else {
                res.json( JSON.parse(JSON.stringify(result)) ); 
            }
            });
});

router.post('/lov-hunter', function(req, res) {
var db = req.con;/* 
const id = req.params.id; */

const id = req.body;

db.query("SELECT lov.lov_id, lovisce.naziv_lovisca, zacetek, konec, plen, lovec_na_lovu.st_strelov \
          FROM lov \
          JOIN lovec_na_lovu ON lovec_na_lovu.lov_lid = lov.lov_id \
          JOIN lovisce on lovisce.lovisce_id = lov.lovisce_id \
          WHERE lovec_na_lovu.uporabnik_id = ?", [id.id], (error, result, fields) => {
            if(!result.length) {
              if(error == null) {
                res.status(201).send("Nimaš preteklih lovov.");
              } else {
                res.status(406).send("Podatkov ni bilo mogoče dobiti.");
              }
            } else {
                res.json( JSON.parse(JSON.stringify(result))); 
            }
            });
});

router.get('/:id', function(req, res) {
const id = req.params.id;
var db = req.con;
db.query("SELECT * \
            FROM lov \
            WHERE lov_id = ?", [id], (error, result, fields) => {
            if(!result.length) {
                res.status(406).send("Podatkov ni bilo mogoče dobiti.");
            } else {
                res.json( result[0] ); 
            }
            });
});

router.post('/add', function(req, res) {
    var db = req.con;
    const { lovisce_id, zacetek } = req.body;
    
    db.query("INSERT INTO lov SET ?", [{ lovisce_id, zacetek }], (error, result, fields) => {
                if(error) {
                  res.status(406).send("Napaka.");
                  console.log("Napaka pri pisanju v bazo.");
                } else {
                  res.status(201).send("" + result.insertId);
                }
        });
});

router.post('/update/:id', function(req, res) {
    const id = req.params.id;
    var db = req.con;
    const { plen, konec, streli } = req.body;
  
    db.query("UPDATE lov \
              SET plen = ?, konec = ? \
              WHERE lov_id = ?", [plen, konec, id], (error, result, fields) => {
                if(error) {
                  res.status(406).send("Napaka.");
                  console.log("Napaka pri pisanju v bazo.");
                  return;
                } else {
                }
        });
    db.query("UPDATE lovec_na_lovu \
              SET st_strelov = ? \
              WHERE lov_lid = ?", [streli, id], (error, result, fields) => {
                if(error) {
                  console.log(error);
                  res.status(406).send("Napaka.");
                  console.log("Napaka pri pisanju v bazo.");
                  return;
                } else {
                  res.status(201).send("Posodobljeno.");
                }
        });
});
module.exports = router;
