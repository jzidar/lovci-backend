var express = require('express');
var router = express.Router();

router.post('/add', function(req, res) {
    var db = req.con;
    const { uporabnik_id, lov_lid } = req.body;
  console.log(uporabnik_id +  " " + lov_lid);
    db.query("INSERT INTO lovec_na_lovu SET ?", [{uporabnik_id, lov_lid}], (error, result, fields) => {
                if(error) {
                  res.status(406).send(error.sqlMessage);
                } else {
                  res.status(201).send("Lov uspešno dodan.");
                }
        });
});

router.get('/allHunters/:id', function(req, res) {
    const id = req.params.id;
    var db = req.con;
    db.query("SELECT uporabnik_id \
                FROM lovec_na_lovu \
                WHERE lov_id = ?", [id], (error, result, fields) => {
                if(!result.length) {
                    res.status(406).send("Podatkov ni bilo mogoče dobiti.");
                    console.log("Podatkov ni bilo mogoče dobiti.");  
                } else {
                    res.json( result[0] ); 
                }
                });
    });

module.exports = router;