var express = require('express');
var router = express.Router();
const { validationResult } = require('express-validator');
var userController = require('../services/service.uporabnik');

const bcrypt = require('bcryptjs');
const saltRounds = 10;

/* uporabnik/login - Method: POST - Prijava avtentikacija uporabnika */
router.post('/login', function(req, res) {
  const { email, password } = req.body;
  var db = req.con;
  if (email && password) {
    db.query("SELECT * \
                  FROM uporabnik \
                  WHERE email = ?", [email], (error, result, fields) => {
                    if(!result.length) {
                      console.log("Uporabnik ne obstaja.");
                      res.status(404).send("Uporabnik ne obstaja.");
                    } else {
                      var hash = result[0].password;        
                      bcrypt.compare(password, hash, function(err, resp) {
                        if(resp == true) {
                          res.status(200).json(JSON.parse(JSON.stringify(result))[0]);
                        } else {
                          res.status(401).send("Prijava ni bila uspešna.");
                          res.end();
                        }
                      });               
                    }
                    }); 
    
  } else {
    console.log("Napaka.");
  }
});

/* uporabnik/info/:id - Method: GET - Pridobitev podatkov uporabnika z id-jem [id] */
router.get('/info/:id', function(req, res) {
  const userId = req.params.id;
  var db = req.con;

  db.query("SELECT * \
            FROM uporabnik \
            WHERE uporabnik_id = ?", [userId], (error, result, fields) => {
              if(!result.length) {
                console.log("Podatkov ni bilo mogoče dobiti.");
                res.status(404).send("Podatkov ni bilo mogoče dobiti.");
              } else {
                res.status(200).json( {uporabnik: JSON.parse(JSON.stringify(result))} );
              }
              });
});

/* uporabnik/registration - Method: POST - Registracija novega uporabnika */
router.post("/registration", userController.validate('noviUporabnik'), async function(req, res) {
  const { tip_id, druzina_id, ime, priimek, email, password1, telefonska } = req.body;
  const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

  if (!errors.isEmpty()) {
    res.status(422).json({ errors: errors.array() });
    console.log("Vneseni parametri niso ustrezni.");
    return;
  }
  console.log(email);
  var db = req.con;

  db.query("SELECT uporabnik_id \
            FROM uporabnik \
            WHERE email = ?", [email], (error, result, fields) => {
                if(error) throw error;
                else {
                  if (result.length == 0) {
                    bcrypt.hash(password1, saltRounds, function(err, password) {  
                      db.query("INSERT INTO uporabnik SET ?", [{tip_id, druzina_id, ime, priimek, password, email, telefonska}], (error, result, fields) => {
                        if(error) {
                          throw error;
                        } else {
                          console.log("Registracija uspešna");
                          res.status(201).json({uporabnik: {tip_id, druzina_id, ime, priimek, email, password, telefonska}});
                        }
                      });
                    });
                  } else {    
                    console.log("Uporabnik že obstaja.");
                    res.status(409).send("Uporabnik že obstaja.");
                  }
                }
            });
});


/* users/update - Method: POST - Posodobitev userja */
router.put("/update/:id", userController.validate('posodobitevUporabnika'), function(req, res, next) {
  const { conf_new_password } = req.body;
  const userId = req.params.id;

  const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

  if (!errors.isEmpty()) {
    res.status(422).json({ errors: errors.array() });
    console.log("Vneseni parametri niso ustrezni.");
    return;
  }
  var db = req.con;

  db.query("UPDATE uporabnik\
            SET password = ?\
            WHERE uporabnik_id = ?", [conf_new_password, userId], (error, result, fields) => {
              if(error) throw error;
              else {
                console.log('Število spremenjenih vrstic: ', result.affectedRows);
                res.status(200).send("OK.");
              }
          });
});

module.exports = router;
