var express = require('express');
var router = express.Router();

router.get('/all', function(req, res) {
  var db = req.con;
  db.query("SELECT * \
            FROM lovisce", (error, result, fields) => {
              if(!result.length) {
                res.status(406).send("Podatkov ni bilo mogoče dobiti.");
                console.log("Podatkov ni bilo mogoče dobiti.");  
              } else {
                res.json( JSON.parse(JSON.stringify(result)) ); 
              }
              });
});


router.get('/:id', function(req, res) {
  const id = req.params.id;
  var db = req.con;
  db.query("SELECT * \
            FROM lovisce \
            WHERE lovisce_id = ?", [id], (error, result, fields) => {
              if(!result.length) {
                res.status(406).send("Podatkov ni bilo mogoče dobiti.");
                console.log("Podatkov ni bilo mogoče dobiti.");  
              } else {
                res.json( result[0] ); 
              }
              });
});

router.post('/add', function(req, res) {
  var db = req.con;
  const { naziv_lovisca, lat, lon } = req.body;

  db.query("INSERT INTO lovisce SET ?", [{naziv_lovisca, lat, lon}], (error, result, fields) => {
              if(error) {
                res.status(406).send("Napaka.");
                console.log("Napaka pri pisanju v bazo.");
              } else {
                res.status(201).send("" + result.insertId);
              }
      });
});

router.post('/update/:id', function(req, res) {
  const id = req.params.id;
  var db = req.con;
  const { naziv_lovisca, lat, lon } = req.body;

  db.query("UPDATE lovisce \
            SET ? \
            WHERE lovisce_id = ?", [{naziv_lovisca, lat, lon}, id], (error, result, fields) => {
              if(error) {
                res.status(406).send("Napaka.");
                console.log("Napaka pri pisanju v bazo.");
              } else {
                res.status(201).send("");
              }
      });
});

module.exports = router;
